﻿using System;
using System.Data;

namespace ExcentOne.Core.Data.Extensions
{
    public static class DataRowExtensions
    {
        public static T ToObject<T>(this DataRow dataRow)
            where T : new()
        {
            var item = new T();
            foreach (DataColumn column in dataRow.Table.Columns)
                if (dataRow[column] != DBNull.Value)
                {
                    var prop = item.GetType().GetProperty(column.ColumnName);
                    if (prop != null)
                    {
                        var result = Convert.ChangeType(dataRow[column], prop.PropertyType);
                        prop.SetValue(item, result, null);
                    }
                    else
                    {
                        var fld = item.GetType().GetField(column.ColumnName);
                        if (fld == null) continue;
                        var result = Convert.ChangeType(dataRow[column], fld.FieldType);
                        fld.SetValue(item, result);
                    }
                }

            return item;
        }
    }
}