﻿using System.Collections.Generic;
using System.IO;

namespace ExcentOne.Core.Data.Extraction
{
    /// <summary>
    /// Exposes operations for extracting data records from a file.
    /// </summary>
    public interface IDataExtractor
    {
        /// <summary>
        /// Extracts a collection of <typeparamref name="TRecord"/> from the file.
        /// </summary>
        /// <typeparam name="TRecord">The type of record.</typeparam>
        /// <param name="fileStream">The stream of file.</param>
        /// <param name="fileName">The name of file.</param>
        /// <returns>A collection of <typeparamref name="TRecord"/>s from the file.</returns>
        ICollection<TRecord> Extract<TRecord>(Stream fileStream, string fileName)
            where TRecord : class, new();

        /// <summary>
        /// Extracts a collection of <typeparamref name="TRecord"/> from the file using the specified <see cref="DataExtractionStrategy"/>.
        /// </summary>
        /// <typeparam name="TRecord">The type of record.</typeparam>
        /// <param name="fileStream">The stream of file.</param>
        /// <param name="fileName">The name of file.</param>
        /// <param name="strategy">The <see cref="DataExtractionStrategy"/> to use for data extraction.</param>
        /// <returns>A collection of <typeparamref name="TRecord"/>s from the file.</returns>
        ICollection<TRecord> Extract<TRecord>(Stream fileStream, string fileName, DataExtractionStrategy strategy)
            where TRecord : class, new();

        /// <summary>
        /// Checks whether the <see cref="IDataExtractor"/> can extract records
        /// from the file using the specified <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file with extension.</param>
        /// <returns><c>true</c> if the <see cref="IDataExtractor"/> can extract records; otherwise <c>false</c>.</returns>
        bool CanExtractFrom(string fileName);
    }
}